import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppConst } from '../../constants/app.const';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  })
};

@Injectable({
  providedIn: 'root'
})
export class DemoService {

  private serverURL:string = AppConst.serverPath;

  constructor(private http: HttpClient) { }

  createUser(user){
    let url = this.serverURL+'users/';
    return this.http.post(url, user, httpOptions) 
  }

  getUsers(){
    let url = this.serverURL+'users/';
    return this.http.get(url, httpOptions);
  }

  getUserDetails(user_id){
    let url = this.serverURL+'users/'+user_id+'/';
    return this.http.get(url, httpOptions); 
  }

  updateUser(user){
    let url = this.serverURL+'users/';
    return this.http.put(url, user, httpOptions) 
  }

  deleteUser(user_id){
    let url = this.serverURL+'users/'+user_id+'/';
    return this.http.delete(url, httpOptions) 
  }

}
