import { Component, OnInit } from '@angular/core';
import { DemoService } from '../../services/demo/demo.service';


@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  new_user: User = new User();
  users: any;
	user_details: any;

  constructor(public _demoService: DemoService) { }

  ngOnInit(): void {
		this.fetchUsers();
  }

  addUser(){
		this._demoService.createUser(this.new_user).subscribe(data=>{
		  this.new_user = new User();
		}, error =>{
			console.log(error);
		})
  }

  fetchUsers(){
		this._demoService.getUsers().subscribe(data=>{
		  this.users = data;
		}, error =>{
			console.log(error);
		});
  }

  fetchUserDetails(user_id){
		this._demoService.getUserDetails(user_id).subscribe(data=>{
		  this.user_details = data;
		}, error =>{
			console.log(error);
		})
  }

  updateUser(user_update){
    this._demoService.updateUser(user_update).subscribe(data=>{
    }, error =>{
			console.log(error);
		})
  }

  deleteUser(user_id){
		this._demoService.deleteUser(user_id).subscribe(data=>{
		  console.log('User deleted !!');
		}, error => {
			console.log(error);
		})
  }
}

export class User{
  first_name: any;
  last_name: any;
  username: any;
  email: any;
}